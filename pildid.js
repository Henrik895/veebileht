const suurPilt = document.querySelector('#suur_pilt');
const pildid = document.querySelectorAll('.pildifailid img');
const läbipaistvus = 0.5;

pildid[0].style.opacity = läbipaistvus;

pildid.forEach(img => img.addEventListener('click', klikkPildil));

function klikkPildil(e) {
  pildid.forEach(img => (img.style.opacity = 1));
  suurPilt.src = e.target.src;

  suurPilt.classList.add('fade-in');
  setTimeout(() => suurPilt.classList.remove('fade-in'), 500);

  e.target.style.opacity = läbipaistvus;
}